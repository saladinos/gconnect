<?php
require_once 'header.php';
define('fortniteApiKey','a743694213d7db7fc94e16ed21caefdd');
 ?>
 <body>

  <div class="container">

    <div class="masthead">
      <h3 class="text-muted"><img style="width:200px" src="<?PATH?>assets/img/800px-Fortnite.png"> API</h3>
    </div>

    <!-- Jumbotron -->
    <div class="jumbotron">
      <h1>Información</h1>
    </div>
  <div class="row">

  <div class="col-sm-6">
  <label for="nombre" class="col-sm-6 col-form-label">No. Guía o Codigo de Rastreo:</label>
   <div class="input-group">
     <input id="guia" type="text" name="guia"  class="form-control" placeholder="EJ: 01201021020111212">
     <span class="input-group-btn">
       <button class="btn btn-primary" type="button" id="buscarPaquete" data-loading-text="buscando...">Rastrear!</button>

     </span>
   </div><!-- /input-group -->
 </div><!-- /.col-lg-6 -->
 <div class="buscando col-sm-1">
   <i class="fa fa-refresh fa-spin fa-2x fa-fw" style="margin-top:45px"></i>
   <span class="sr-only">buscando...</span>
 </div>
  </div>
<div class="page-header" style="margin-top:30px;">
  <h1>Detalles de su pedido.</h1>
</div>
<div class="col-xs-12" style="margin-top:40px;">
    <div class="form-group row">
      <label for="numero_guia" class="col-sm-1 col-form-label">No. Guia</label>
          <div class="col-sm-8">
        <input type="text" name="numero_guia" class="imprimir form-control" placeholder="" readonly  />
        </div>
    </div>
    <div class="form-group row">
      <label for="origen" class="col-sm-1 col-form-label">Origen</label>
      <div class="col-sm-2">
        <input type="text" name="origen" class="origen form-control" placeholder="" readonly  />
      </div>
      <label for="destino" class="col-sm-1 col-form-label">Destino</label>
      <div class="col-sm-2">
        <input type="text" name="destino" class="destino form-control" placeholder="" readonly  />
      </div>
      <label for="estatus" class="col-sm-1 col-form-label">Estatus</label>
      <div class="col-sm-2">
        <input type="text" name="estatus" class="imprimir form-control" placeholder="" readonly  />
      </div>
    </div>
    <div class="form-group row">
      <label for="fecha_entrega" class="col-sm-1   col-form-label">Entrega</label>
      <div class="col-sm-3">
        <input type="text" name="fecha_entrega" class="imprimir form-control" placeholder="" readonly  />
      </div>
      <label for="recibio" class="col-sm-1 col-form-label">Recibió</label>
      <div class="col-sm-3">
        <input type="text" name="recibio" class="imprimir form-control" placeholder="" readonly  />
      </div>
    </div>
    <div class="movimientos">
      <div class="table-info">
  <div class="table-header">
    <div class="table-caption">
      Seguimiento del pedido
    </div>
  </div>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Fecha</th>
        <th>Descripción</th>
      </tr>
    </thead>
    <tbody id="movimientos">
    </tbody>
  </table>
  <div class="table-footer">
    Ejemplo No. Guía <kbd>9058553770552701607838</kbd>
  </div>
</div>

    </div>
</div>



  <?php
require_once 'footer.php'
   ?>
