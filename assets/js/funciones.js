var $btn;
$('#buscarPaquete').on('click', function(){
  btn = $(this);
  btn.val(btn.data("loading-text"));
	buscaEnvio();
});
function buscaEnvio() {
  $(".input-error").remove();
  var datos = {"guia" : $('#guia').val()};
   $(".buscando").show();
    $.ajax({
      url: PATH+'controller/rastreo.php',
      type: 'POST',
      data: datos
    })
    .done(function(respuesta) {
      var rastreo = JSON.parse(respuesta);
      if(typeof rastreo.error == "undefined"){
        $('.imprimir').each(function(i, el){
          _input = $(el);
          _name = _input.attr('name');
          _input.val(rastreo[_name]);
        });
        $('.origen').val(rastreo.origen.nombre);
        $('.destino').val(rastreo.destino.nombre);
        var movimientos = "";
        $(rastreo.movimientos).each(function(i, el){
          movimientos += "<tr>";
          movimientos += '<td>' + el.fecha + '</td>';
          movimientos += '<td>' + el.descripcion + '</td>';
          movimientos += "</tr>";
        });
        $('#movimientos').append(movimientos);
        btn.val('Rastrear!');
        $(".buscando").hide();

      }else{
        $.growl.error({title: 'Error', message: rastreo.mensaje_error, duration:5000 });
        btn.val('Rastrear!');
        $(".buscando").hide();
    }
  });
}
